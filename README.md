# Kismet Prometheus Exporter

This exporter collects metrics from Kismet API and presents them as Prometheus metrics.

Metrics collected:

- System Information:
  - Server timestamp
  - Used memory
  - HTTP connection to the API
  - Temperature sensors
  - Battery info
  - Total detected devices by Kismet
- Alerts raised by Kismet
- Per Channel (Frequency):
  - Devices detected
  - Packets collected
  - Data bytes collected
  - Signal levels
- Per data source:
  - If any warnings present
  - If any errors present
  - Data source retry attempts (retries to start the source)
  - Data source driver capabilities
  - Data source settings (enabled, disabled, channels listened etc.)
  - Active channels the data source listens on

## Start

To run the exporter you need to pass parameters below as environment variables:

| Name                        | Description                                                                                        | Optional                |
| --------------------------- | -------------------------------------------------------------------------------------------------- | ----------------------- |
| KISMET_SERVER_URL           | URL to connect to Kismet server API                                                                | `False`                 |
| KISMET_API_TOKEN            | API Token to connect to Kismet (readonly access is enough)                                         | `False`                 |
| EXPORTER_BIND_SOCKET        | Golang format to describe on which TCP port and host the exporter listens to                       | `True`, default `:9091` |
| EXPORTER_ALERTS_LOOKUP_BACK | Golang time.Duration string to retrieve Kismet Alerts which were raised the Duration back from now | `True`, default `1m`    |

then you should compile `cmd` package and run it. The metrics are available at `/metrics` path.
