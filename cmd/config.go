package main

import (
	"errors"
	"fmt"
	"net/url"
	"time"

	"github.com/spf13/viper"
)

var ErrIncorrectValue = errors.New("incorrect value")

const (
	envVarPrefix = "KISMET_EXPORTER"

	envVarEventsRetention = "EVENTS_RETENTION"
	envVarBind            = "BIND"
	envVarAPIToken        = "API_TOKEN"
	envVarServerURL       = "SERVER_URL"
)

type exporterConfig struct {
	ServerURL       *url.URL
	ServerURLRaw    string
	APIToken        string
	BindSocket      string
	EventsRetention time.Duration
}

func parseConfig() (*exporterConfig, error) {
	viper.SetDefault(envVarBind, ":9091")
	viper.SetDefault(envVarEventsRetention, time.Minute)

	viper.SetEnvPrefix(envVarPrefix)

	if err := viper.BindEnv(envVarServerURL); err != nil {
		return nil, fmt.Errorf("can not configure environment variable keys: %w", err)
	}

	if err := viper.BindEnv(envVarAPIToken); err != nil {
		return nil, fmt.Errorf("can not configure environment variable keys: %w", err)
	}

	if err := viper.BindEnv(envVarBind); err != nil {
		return nil, fmt.Errorf("can not configure environment variable keys: %w", err)
	}

	if err := viper.BindEnv(envVarEventsRetention); err != nil {
		return nil, fmt.Errorf("can not configure environment variable keys: %w", err)
	}

	var cfg exporterConfig

	cfg.ServerURLRaw = viper.GetString(envVarServerURL)
	cfg.APIToken = viper.GetString(envVarAPIToken)
	cfg.BindSocket = viper.GetString(envVarBind)
	cfg.EventsRetention = viper.GetDuration(envVarEventsRetention)

	if cfg.APIToken == "" {
		return nil, fmt.Errorf("%w: %s_%s is empty", ErrIncorrectValue, envVarPrefix, envVarAPIToken)
	}

	if cfg.BindSocket == "" {
		return nil, fmt.Errorf("%w: %s_%s is empty", ErrIncorrectValue, envVarPrefix, envVarBind)
	}

	if cfg.EventsRetention < time.Second {
		return nil, fmt.Errorf("%w: %s_%s is less than 1 second", ErrIncorrectValue, envVarPrefix, envVarEventsRetention)
	}

	u, err := url.Parse(cfg.ServerURLRaw)
	if err != nil {
		return nil, fmt.Errorf("can not parse API server URL from environment variable %s_%s: %w",
			envVarPrefix, envVarServerURL, err)
	}

	cfg.ServerURL = u

	return &cfg, nil
}
