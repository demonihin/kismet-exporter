package main

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	klog "k8s.io/klog/v2"

	kismet "gitlab.com/demonihin/go-kismet-api"
	"gitlab.com/demonihin/kismet-exporter/metrics"
	"gitlab.com/demonihin/kismet-exporter/metrics/devices"
)

func main() {
	klog.Info("Starting Kismet exporter")

	cfg, err := parseConfig()
	if err != nil {
		klog.Exitln(err)
	}

	klog.InfoS(
		"Start parameters",
		"Kismet server URL", cfg.ServerURL,
		"Kismet API Token", "present",
		"Metrics bind socket", cfg.BindSocket,
		"Events retention", cfg.EventsRetention,
		"Metrics path", "/metrics",
		"Health path", "/health",
	)

	kClient, err := kismet.New(cfg.ServerURL, cfg.APIToken)
	if err != nil {
		klog.Fatalln(err)
	}

	var kismetMetrics = metrics.New(kClient,
		metrics.AlertsMetricsOptions{Retention: cfg.EventsRetention, EnableMACAddresses: true},
		devices.MetricsOptions{Retention: cfg.EventsRetention, ListDot11SSIDs: true, ListDot11Clients: true},
	)

	prometheus.DefaultRegisterer.MustRegister(kismetMetrics)

	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/health", kismetMetrics.HealthHandler)

	log.Fatalln(http.ListenAndServe(cfg.BindSocket, nil))
}
