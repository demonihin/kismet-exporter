package metrics

import (
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
	kismet "gitlab.com/demonihin/go-kismet-api"
	"gitlab.com/demonihin/kismet-exporter/metrics/helpers"
	klog "k8s.io/klog/v2"
)

const datasourcesMetricsCollectorSubsystem = "datasources"

type datasourcesMetricsCollector struct {
	kismetClient *kismet.Client

	// warnings - 1 if "kismet.datasource.warning" present.
	warnings prometheus.GaugeVec
	// errors - code "kismet.datasource.error".
	errors prometheus.GaugeVec

	// totalRetryAttempts - value of "kismet.datasource.total_retry_attempts".
	totalRetryAttempts prometheus.GaugeVec

	// driverCapabilities - "kismet.datasource.type_driver".
	driverCapabilities prometheus.GaugeVec

	datasourceSettings prometheus.GaugeVec

	// datasourceChannels - active channels which are scanned by Kismet.
	datasourceChannels prometheus.GaugeVec

	packetCounters prometheus.GaugeVec
}

func newDatasourcesMetricsCollector(cl *kismet.Client) *datasourcesMetricsCollector {
	var commonLabels = []string{
		"capture_interface",
		"interface",
		"hardware",
		"name",
		"uuid",
	}

	return &datasourcesMetricsCollector{
		kismetClient: cl,
		warnings: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: MetricsNamespace,
				Subsystem: datasourcesMetricsCollectorSubsystem,
				Name:      "warning",
				Help:      "Warning present",
			},
			commonLabels,
		),
		errors: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: MetricsNamespace,
				Subsystem: datasourcesMetricsCollectorSubsystem,
				Name:      "error",
				Help:      "Error code; 0 - no error",
			},
			commonLabels,
		),
		totalRetryAttempts: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: MetricsNamespace,
				Subsystem: datasourcesMetricsCollectorSubsystem,
				Name:      "total_retry_attempts",
				Help:      "Count of retry attempts for a Datasource",
			},
			commonLabels,
		),
		driverCapabilities: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: MetricsNamespace,
				Subsystem: datasourcesMetricsCollectorSubsystem,
				Name:      "driver_capabilities",
				Help:      "Capture driver capabilities",
			},
			helpers.AppendCopyLabels(commonLabels, []string{"capability"}),
		),
		datasourceSettings: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: MetricsNamespace,
				Subsystem: datasourcesMetricsCollectorSubsystem,
				Name:      "settings",
				Help:      "Configured settings",
			},
			helpers.AppendCopyLabels(commonLabels, []string{"configuration_parameter"}),
		),
		packetCounters: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: MetricsNamespace,
				Subsystem: datasourcesMetricsCollectorSubsystem,
				Name:      "packets",
				Help:      "Packet counters",
			},
			helpers.AppendCopyLabels(commonLabels, []string{"type"}),
		),
		datasourceChannels: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: MetricsNamespace,
				Subsystem: datasourcesMetricsCollectorSubsystem,
				Name:      "channels",
				Help:      "Enabled channels list",
			},
			helpers.AppendCopyLabels(commonLabels, []string{"channel_name"}),
		),
	}
}

var _ prometheus.Collector = (*datasourcesMetricsCollector)(nil)

func (ds *datasourcesMetricsCollector) Describe(descs chan<- *prometheus.Desc) {
	ds.datasourceSettings.Describe(descs)
	ds.driverCapabilities.Describe(descs)
	ds.errors.Describe(descs)
	ds.packetCounters.Describe(descs)
	ds.totalRetryAttempts.Describe(descs)
	ds.warnings.Describe(descs)
}

func (ds *datasourcesMetricsCollector) Collect(metrics chan<- prometheus.Metric) {
	if err := ds.collectDatasources(metrics); err != nil {
		klog.ErrorS(err, "collectDatasources")
	}
}

func (ds *datasourcesMetricsCollector) collectDatasources(metrics chan<- prometheus.Metric) error {
	datasources, err := ds.kismetClient.GetDatasourcesAll()
	if err != nil {
		// Report error.
		metrics <- prometheus.NewInvalidMetric(
			prometheus.NewDesc(
				MetricsNamespace+"_"+datasourcesMetricsCollectorSubsystem,
				"Kismet API call error",
				nil,
				prometheus.Labels{},
			),
			err,
		)

		return fmt.Errorf("kismet.GetDatasourcesAll: %w", err)
	}

	//nolint:lll // Long lines, I think, are more readable here because one line == one metric.
	for sourceInd := range datasources {
		var source = datasources[sourceInd]

		ds.warnings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID).Set(boolToFloat(source.Warning != ""))
		ds.errors.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID).Set(float64(source.Error))
		ds.totalRetryAttempts.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID).Set(float64(source.TotalRetryAttempts))

		ds.packetCounters.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "error").Set(float64(source.NumErrorPackets))
		ds.packetCounters.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "total").Set(float64(source.NumPackets))

		// Driver capabilities.
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "hop_capable").Set(boolToFloat(source.TypeDriver.HopCapable))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "tuning_capable").Set(boolToFloat(source.TypeDriver.TuningCapable))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "passive_capable").Set(boolToFloat(source.TypeDriver.PassiveCapable))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "remote_capable").Set(boolToFloat(source.TypeDriver.RemoteCapable))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "local_ipc").Set(boolToFloat(source.TypeDriver.LocalIPC))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "local_capable").Set(boolToFloat(source.TypeDriver.LocalCapable))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "list_ipc").Set(boolToFloat(source.TypeDriver.ListIPC))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "list_capable").Set(boolToFloat(source.TypeDriver.ListCapable))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "probe_ipc").Set(boolToFloat(source.TypeDriver.ProbeIPC))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "probe_capable").Set(boolToFloat(source.TypeDriver.ProbeCapable))
		ds.driverCapabilities.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "tuning_capable").Set(boolToFloat(source.TypeDriver.TuningCapable))

		// Datasource settings.
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "hopping").Set(boolToFloat(source.Hopping))
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "passive").Set(boolToFloat(source.Passive))
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "remote").Set(boolToFloat(source.Remote))
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "running").Set(boolToFloat(source.Running))
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "paused").Set(boolToFloat(source.Paused))
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "hop_rate").Set(float64(source.HopRate))
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "hop_split").Set(float64(source.HopSplit))
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "hop_offset").Set(float64(source.HopOffset))
		ds.datasourceSettings.WithLabelValues(source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID, "hop_shuffle").Set(boolToFloat(source.HopShuffle))

		// Channels.
		// Kismet returns channels in the same order for available and "hop" channels.
		if source.Hopping {
			channelStatuses := channelsEnabled(source.Channels, source.HopChannels)

			for ind, st := range channelStatuses {
				ds.datasourceChannels.WithLabelValues(source.Channels[ind], source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID).Set(st)
			}
		} else {
			for _, chName := range source.Channels {
				ds.datasourceChannels.WithLabelValues(chName, source.CaptureInterface, source.Interface, source.Hardware, source.Name, source.UUID).Set(boolToFloat(chName == source.Channel))
			}
		}
	}

	ds.warnings.Collect(metrics)
	ds.errors.Collect(metrics)
	ds.packetCounters.Collect(metrics)
	ds.driverCapabilities.Collect(metrics)
	ds.datasourceSettings.Collect(metrics)
	ds.datasourceChannels.Collect(metrics)

	return nil
}

// channelsEnabled - returns statuses of all channels based on their enabled/disabled status
// i.e. presence in enabled array.
// Returned array has the same size as all and contains one of [0, 1]
// for each item in all where 1 - exists in enabled, 0 - does not exist.
func channelsEnabled(all, enabled []string) []float64 {
	// Kismet returns channels in the same order for available and "hop" channels.
	// So we use it.
	var (
		enInd int

		result = make([]float64, len(all))
	)

	for ind, allChan := range all {
		if allChan == enabled[enInd] {
			result[ind] = 1

			enInd++
		} else {
			result[ind] = 0
		}

		if enInd == len(enabled) {
			break
		}
	}

	return result
}

func boolToFloat(b bool) float64 {
	if b {
		return 1
	}

	return 0
}
