package helpers

func AppendCopyLabels(commonLabels, labels []string) []string {
	var cp = make([]string, 0, len(commonLabels)+len(labels))

	cp = append(cp, commonLabels...)
	cp = append(cp, labels...)

	return cp
}
