package metrics

import (
	"fmt"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	kismet "gitlab.com/demonihin/go-kismet-api"
	klog "k8s.io/klog/v2"
)

const alertsMetricsCollectorSubsystem = "alerts"

type AlertsMetricsOptions struct {
	// Retention - means:
	// get current time and deduct the value (time.Now().Add(-Retention)).
	Retention time.Duration

	// EnableMACAddresses - annotates alerts with transmitter, receiver MAC addresses.
	// This may cause
	// too high load on Kismet, exporter and Prometheus
	// because the cardinality of metrics might change very often.
	EnableMACAddresses bool
}

type alertsMetricsCollector struct {
	kismetClient *kismet.Client

	options AlertsMetricsOptions

	alerts prometheus.GaugeVec
}

func newAlertsMetricsCollector(cl *kismet.Client, options AlertsMetricsOptions) *alertsMetricsCollector {
	var metricLabels = []string{
		"device_key",
		"header",
		"class",
		"severity",
		"severity_text",
		"channel",
		"frequency",
	}

	if options.EnableMACAddresses {
		metricLabels = append(metricLabels, []string{
			"transmitter_mac",
			"source_mac",
			"dest_mac",
			"other_mac",
		}...)
	}

	return &alertsMetricsCollector{
		kismetClient: cl,
		options:      options,
		alerts: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: MetricsNamespace,
				Subsystem: alertsMetricsCollectorSubsystem,
				Name:      "last_seen",
				Help:      fmt.Sprintf("Alerts appeared during %s before the query", options.Retention.String()),
			},
			metricLabels,
		),
	}
}

var _ prometheus.Collector = (*alertsMetricsCollector)(nil)

func (am *alertsMetricsCollector) Describe(descs chan<- *prometheus.Desc) {
	am.alerts.MetricVec.Describe(descs)
}

func (am *alertsMetricsCollector) Collect(metrics chan<- prometheus.Metric) {
	if err := am.collectAlerts(metrics); err != nil {
		klog.ErrorS(err, "collectAlerts")
	}
}

func (am *alertsMetricsCollector) collectAlerts(metrics chan<- prometheus.Metric) error {
	ts := time.Now().Add(-am.options.Retention)

	alerts, err := am.kismetClient.GetAlertsLastSeen(kismet.TimeUSec{Seconds: ts.Unix()})
	if err != nil {
		// Report error.
		metrics <- prometheus.NewInvalidMetric(
			prometheus.NewDesc(
				MetricsNamespace+"_"+alertsMetricsCollectorSubsystem,
				"Kismet API call error",
				nil,
				prometheus.Labels{},
			),
			err,
		)

		return fmt.Errorf("kismet.GetAlertsLastSeen: %w", err)
	}

	// Reset.
	am.alerts.Reset()

	// Populate.
	for i := range alerts {
		var alert = &alerts[i]

		/*
			"device_key",
			"header",
			"class",
			"severity",
			"severity_text",
			"channel",
			"frequency",
		*/
		var labelValues = []string{
			alert.DeviceKey,
			alert.Header,
			string(alert.Class),
			strconv.FormatInt(int64(alert.Severity), 10), //nolint:gomnd // we use base 10 numbers.
			alert.Severity.String(),
			alert.Channel,
			strconv.FormatInt(alert.Frequency, 10), //nolint:gomnd // we use base 10 numbers.
		}

		/*
			"transmitter_mac",
			"source_mac",
			"dest_mac",
			"other_mac",
		*/
		if am.options.EnableMACAddresses {
			labelValues = append(labelValues, []string{
				alert.TransmitterMAC,
				alert.SourceMAC,
				alert.DestMAC,
				alert.OtherMAC,
			}...)
		}

		am.alerts.WithLabelValues(labelValues...).Add(1)
	}

	// Collect.
	am.alerts.Collect(metrics)

	return nil
}
