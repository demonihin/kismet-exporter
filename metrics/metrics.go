package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	kismet "gitlab.com/demonihin/go-kismet-api"
	"gitlab.com/demonihin/kismet-exporter/metrics/devices"
)

const MetricsNamespace = "kismet"

type Client struct {
	kismetAPI *kismet.Client

	system      *systemMetricsCollector
	channels    *channelsMetricsCollector
	alerts      *alertsMetricsCollector
	datasources *datasourcesMetricsCollector
	devices     *devices.MetricsCollector
}

func New(client *kismet.Client, alertsOptions AlertsMetricsOptions, devicesOptions devices.MetricsOptions) *Client {
	return &Client{
		kismetAPI:   client,
		system:      newSystemMetricsCollector(client),
		channels:    newChannelsMetricsCollector(client),
		alerts:      newAlertsMetricsCollector(client, alertsOptions),
		datasources: newDatasourcesMetricsCollector(client),
		devices:     devices.NewMetricsCollector(client, MetricsNamespace, devicesOptions),
	}
}

var _ prometheus.Collector = (*Client)(nil)

// Describe sends the super-set of all possible descriptors of metrics
// collected by this Collector to the provided channel and returns once
// the last descriptor has been sent. The sent descriptors fulfill the
// consistency and uniqueness requirements described in the Desc
// documentation.
//
// It is valid if one and the same Collector sends duplicate
// descriptors. Those duplicates are simply ignored. However, two
// different Collectors must not send duplicate descriptors.
//
// Sending no descriptor at all marks the Collector as “unchecked”,
// i.e. no checks will be performed at registration time, and the
// Collector may yield any Metric it sees fit in its Collect method.
//
// This method idempotently sends the same descriptors throughout the
// lifetime of the Collector. It may be called concurrently and
// therefore must be implemented in a concurrency safe way.
//
// If a Collector encounters an error while executing this method, it
// must send an invalid descriptor (created with NewInvalidDesc) to
// signal the error to the registry.
func (cl *Client) Describe(dchan chan<- *prometheus.Desc) {
	cl.system.Describe(dchan)
	cl.channels.Describe(dchan)
	cl.alerts.Describe(dchan)
	cl.datasources.Describe(dchan)
	cl.devices.Describe(dchan)
}

// Collect is called by the Prometheus registry when collecting
// metrics. The implementation sends each collected metric via the
// provided channel and returns once the last metric has been sent. The
// descriptor of each sent metric is one of those returned by Describe
// (unless the Collector is unchecked, see above). Returned metrics that
// share the same descriptor must differ in their variable label
// values.
//
// This method may be called concurrently and must therefore be
// implemented in a concurrency safe way. Blocking occurs at the expense
// of total performance of rendering all registered metrics. Ideally,
// Collector implementations support concurrent readers.
func (cl *Client) Collect(mchan chan<- prometheus.Metric) {
	cl.system.Collect(mchan)
	cl.channels.Collect(mchan)
	cl.alerts.Collect(mchan)
	cl.datasources.Collect(mchan)
	cl.devices.Collect(mchan)
}

var _ http.HandlerFunc = (*Client)(nil).HealthHandler

// HealthHandler - health check handler.
func (cl *Client) HealthHandler(w http.ResponseWriter, req *http.Request) {
	if _, err := cl.kismetAPI.GetSystemTimestamp(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusOK)
}
