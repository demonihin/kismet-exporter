package devices

import "github.com/prometheus/client_golang/prometheus"

type bluetoothDeviceMetrics struct {
	deviceType prometheus.GaugeVec
	pathLoss   prometheus.GaugeVec
}

func (dbdm *bluetoothDeviceMetrics) Describe(descs chan<- *prometheus.Desc) {
	dbdm.deviceType.Describe(descs)
	dbdm.pathLoss.Describe(descs)
}

func newBluetoothDeviceMetrics(metricsNamespace string, devicesMetricLabels []string) bluetoothDeviceMetrics {
	return bluetoothDeviceMetrics{
		deviceType: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsBluetoothSubsystem,
				Name:      "device_type",
				Help:      "Bluetooth device type",
			},
			devicesMetricLabels,
		),
		pathLoss: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsBluetoothSubsystem,
				Name:      "path_loss",
				Help:      "Bluetooth path loss",
			},
			devicesMetricLabels,
		),
	}
}
