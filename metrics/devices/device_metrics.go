package devices

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/demonihin/kismet-exporter/metrics/helpers"
)

type metrics struct {
	numAlerts  prometheus.GaugeVec
	timestamps prometheus.CounterVec
	frequency  prometheus.GaugeVec
	freqHzMap  prometheus.CounterVec
	txPower    prometheus.GaugeVec
	signal     prometheus.GaugeVec
	// packets - also include 802.11 retries counter.
	packets prometheus.CounterVec

	datasize prometheus.CounterVec

	dot11DeviceMetrics     dot11DeviceMetrics
	bluetoothDeviceMetrics bluetoothDeviceMetrics
}

func (ddm *metrics) Describe(descs chan<- *prometheus.Desc) {
	ddm.numAlerts.Describe(descs)
	ddm.timestamps.Describe(descs)
	ddm.frequency.Describe(descs)
	ddm.freqHzMap.Describe(descs)
	ddm.txPower.Describe(descs)
	ddm.signal.Describe(descs)
	ddm.packets.Describe(descs)
	ddm.datasize.Describe(descs)

	ddm.dot11DeviceMetrics.Describe(descs)
	ddm.bluetoothDeviceMetrics.Describe(descs)
}

func newDeviceMetrics(metricsNamespace string, options MetricsOptions) *metrics {
	var devicesMetricLabels = []string{
		"device_name",
		"device_common_name",
		"device_phy_name",
		"device_type",
		"device_manufacturer",
		"device_mac_address",
		"device_key",
	}

	return &metrics{
		numAlerts: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsSubsystem,
				Name:      "num_alerts",
				Help:      "Number of Alerts related to this device",
			},
			devicesMetricLabels,
		),
		timestamps: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsSubsystem,
				Name:      "event_timestamp",
				Help:      "Timestamps of first, last, mod time, BSS, last beacon etc.",
			},
			helpers.AppendCopyLabels(devicesMetricLabels, []string{"timestamp_name"}),
		),
		frequency: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsSubsystem,
				Name:      "frequency",
				Help:      "Main frequency at which this device works",
			},
			devicesMetricLabels,
		),
		freqHzMap: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsSubsystem,
				Name:      "packets_per_frequency",
				Help:      "Number of packets seen at each frequency",
			},
			helpers.AppendCopyLabels(devicesMetricLabels, []string{"frequency"}),
		),
		signal: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsSubsystem,
				Name:      "common_signal",
				Help:      "Signal common metrics",
			},
			helpers.AppendCopyLabels(devicesMetricLabels, []string{"signal_type"}),
		),
		packets: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsSubsystem,
				Name:      "packets",
				Help:      "Packet counts by type, including 802.11 fragments and retries",
			},
			helpers.AppendCopyLabels(devicesMetricLabels, []string{"packets_type"}),
		),
		datasize: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsSubsystem,
				Name:      "datasize",
				Help:      "Total transmitted bytes",
			},
			devicesMetricLabels,
		),
		txPower: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsSubsystem,
				Name:      "tx_power",
			},
			devicesMetricLabels,
		),
		dot11DeviceMetrics:     newDot11DeviceMetrics(metricsNamespace, devicesMetricLabels, options),
		bluetoothDeviceMetrics: newBluetoothDeviceMetrics(metricsNamespace, devicesMetricLabels),
	}
}
