package devices

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/demonihin/kismet-exporter/metrics/helpers"
)

type dot11DeviceSSIDsMetrics struct {
	metadata prometheus.Metric

	ciscoClientMFP               prometheus.GaugeVec
	ccxTxpower                   prometheus.GaugeVec
	dot11EChannelUtilizationPerc prometheus.GaugeVec
	dot11EQbssStations           prometheus.GaugeVec
	dot11EQbss                   prometheus.GaugeVec
	dot11MobilityDomainID        prometheus.GaugeVec
	dot11RMobility               prometheus.GaugeVec

	timestamps prometheus.CounterVec

	dhtCenter1 prometheus.GaugeVec
	dhtCenter2 prometheus.GaugeVec

	wpsVersion      prometheus.GaugeVec
	idCloaked       prometheus.GaugeVec
	maxRate         prometheus.GaugeVec
	beaconRate      prometheus.GaugeVec
	beaconsSec      prometheus.GaugeVec
	wpaMFPRequired  prometheus.GaugeVec
	wpaMFPSupported prometheus.GaugeVec
}

func (ddSSIDsM *dot11DeviceSSIDsMetrics) Describe(descs chan<- *prometheus.Desc) {
	ddSSIDsM.beaconRate.Describe(descs)
	ddSSIDsM.beaconsSec.Describe(descs)
	ddSSIDsM.ccxTxpower.Describe(descs)
	ddSSIDsM.ciscoClientMFP.Describe(descs)
	ddSSIDsM.dhtCenter1.Describe(descs)
	ddSSIDsM.dhtCenter2.Describe(descs)
	ddSSIDsM.dot11EChannelUtilizationPerc.Describe(descs)
	ddSSIDsM.dot11EQbss.Describe(descs)
	ddSSIDsM.dot11EQbssStations.Describe(descs)
	ddSSIDsM.dot11MobilityDomainID.Describe(descs)
	ddSSIDsM.dot11RMobility.Describe(descs)
	ddSSIDsM.idCloaked.Describe(descs)
	ddSSIDsM.maxRate.Describe(descs)
	// ddSSIDsM.metadata.Describe(descs)
	ddSSIDsM.timestamps.Describe(descs)
	ddSSIDsM.wpaMFPRequired.Describe(descs)
	ddSSIDsM.wpaMFPSupported.Describe(descs)
	ddSSIDsM.wpsVersion.Describe(descs)
}

func newDot11DeviceSSIDsMetrics( //nolint:funlen
	metricsNamespace string, devicesMetricLabels []string) *dot11DeviceSSIDsMetrics {
	var ssidMetricLabels = helpers.AppendCopyLabels(devicesMetricLabels, []string{"ssid"})

	return &dot11DeviceSSIDsMetrics{
		ciscoClientMFP: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "client_mfp",
				Help:      "Cisco client management frame protection flag",
			},
			ssidMetricLabels,
		),
		ccxTxpower: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "ccx_tx_power",
				Help:      "Cisco advertised TX power",
			},
			ssidMetricLabels,
		),
		dot11EChannelUtilizationPerc: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "dot11e_channel_util_perc",
				Help:      "QBSS reported channel utilization",
			},
			ssidMetricLabels,
		),
		dot11EQbssStations: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "dot11e_qbss_stations",
				Help:      "QBSS stations count",
			},
			ssidMetricLabels,
		),
		dot11EQbss: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "dot11e_qbss",
				Help:      "Is QBSS support announced",
			},
			ssidMetricLabels,
		),
		dot11MobilityDomainID: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "dot11r_mobility_domain_id",
				Help:      "Advertised dot11r mobility domain id",
			},
			ssidMetricLabels,
		),
		dot11RMobility: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "dot11r_mobility_support",
				Help:      "Is dot11r mobility support announced",
			},
			ssidMetricLabels,
		),
		dhtCenter1: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "ht_center_1",
				Help:      "HT/VHT Center Frequency (primary)",
			},
			ssidMetricLabels,
		),
		dhtCenter2: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "ht_center_2",
				Help:      "HT/VHT Center Frequency (secondary, for 80+80 Wave2)",
			},
			ssidMetricLabels,
		),
		wpsVersion: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "wps_version",
				Help:      "Advertised WPS version",
			},
			ssidMetricLabels,
		),
		idCloaked: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "id_cloaked",
				Help:      "Is SSID hidden / cloaked",
			},
			ssidMetricLabels,
		),
		maxRate: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "max_rate",
				Help:      "Advertised maximum rate",
			},
			ssidMetricLabels,
		),
		beaconRate: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "beacon_rate",
				Help:      "Beacon rate",
			},
			ssidMetricLabels,
		),
		beaconsSec: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "beacons_sec",
				Help:      "Beacons seen during last 1 second",
			},
			ssidMetricLabels,
		),
		wpaMFPRequired: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "wpa_mfp_required",
				Help:      "Is WPA management protection required",
			},
			ssidMetricLabels,
		),
		wpaMFPSupported: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "wpa_mfp_supported",
				Help:      "Is WPA management protection supported",
			},
			ssidMetricLabels,
		),
		timestamps: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11SSIDsSubsystem,
				Name:      "event_timestamp",
				Help:      "First, last seen and other timetamps",
			},
			helpers.AppendCopyLabels(ssidMetricLabels, []string{"timestamp_name"}),
		),
	}
}
