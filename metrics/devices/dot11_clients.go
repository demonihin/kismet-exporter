package devices

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/demonihin/kismet-exporter/metrics/helpers"
)

type dot11DeviceClientsMetrics struct {
	packets       prometheus.CounterVec
	datasizeRetry prometheus.CounterVec
	datasize      prometheus.CounterVec
	timestamps    prometheus.CounterVec
}

func (dDDCM *dot11DeviceClientsMetrics) Describe(descs chan<- *prometheus.Desc) {
	dDDCM.datasize.Describe(descs)
	dDDCM.datasizeRetry.Describe(descs)
	dDDCM.packets.Describe(descs)
	dDDCM.timestamps.Describe(descs)
}

func newDot11DeviceClientsMetrics(metricsNamespace string, devicesMetricLabels []string) *dot11DeviceClientsMetrics {
	var clientsMetricLabels = helpers.AppendCopyLabels(devicesMetricLabels, []string{"bssid", "bssid_key"})

	return &dot11DeviceClientsMetrics{
		packets: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11ClientsSubsystem,
				Name:      "packets",
				Help:      "Client packets counter",
			},
			helpers.AppendCopyLabels(clientsMetricLabels, []string{"packets_type"}),
		),
		datasizeRetry: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11ClientsSubsystem,
				Name:      "datasize_retry",
				Help:      "Total retry bytes",
			},
			clientsMetricLabels,
		),
		datasize: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11ClientsSubsystem,
				Name:      "datasize",
				Help:      "Total transmitted bytes",
			},
			clientsMetricLabels,
		),
		timestamps: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11ClientsSubsystem,
				Name:      "timestamps",
				Help:      "Timestamps of first, last seen",
			},
			helpers.AppendCopyLabels(clientsMetricLabels, []string{"event_type"}),
		),
	}
}
