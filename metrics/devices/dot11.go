package devices

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/demonihin/kismet-exporter/metrics/helpers"
)

type dot11DeviceMetrics struct {
	datasizeRetry prometheus.CounterVec
	// packets - also include 802.11 retries counter.
	// packets prometheus.CounterVec

	numAssociatedClients prometheus.GaugeVec
	numSSIDs             prometheus.GaugeVec

	clientDisconnects prometheus.CounterVec

	// txPower prometheus.GaugeVec

	advertisedSSIDs *dot11DeviceSSIDsMetrics
	clients         *dot11DeviceClientsMetrics
}

func (dDDM *dot11DeviceMetrics) Describe(descs chan<- *prometheus.Desc) {
	dDDM.datasizeRetry.Describe(descs)
	dDDM.numAssociatedClients.Describe(descs)
	dDDM.numSSIDs.Describe(descs)
	dDDM.clientDisconnects.Describe(descs)

	if dDDM.advertisedSSIDs != nil {
		dDDM.advertisedSSIDs.Describe(descs)
	}

	if dDDM.clients != nil {
		dDDM.clients.Describe(descs)
	}
}

func newDot11DeviceMetrics(
	metricsNamespace string, devicesMetricLabels []string, options MetricsOptions) dot11DeviceMetrics {
	var metrics = dot11DeviceMetrics{
		datasizeRetry: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11Subsystem,
				Name:      "datasize_retry",
				Help:      "Total transmitted retries",
			},
			devicesMetricLabels,
		),
		numAssociatedClients: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11Subsystem,
				Name:      "associated_clients",
				Help:      "Number of associated clients",
			},
			devicesMetricLabels,
		),
		numSSIDs: *prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11Subsystem,
				Name:      "ssids",
				Help:      "Number of SSIDs by type (advertised, responded, probed)",
			},
			helpers.AppendCopyLabels(devicesMetricLabels, []string{"ssid_type"}),
		),
		clientDisconnects: *prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: metricsNamespace,
				Subsystem: devicesMetricsDot11Subsystem,
				Name:      "client_disconnects",
				Help:      "Client disconnects count",
			},
			devicesMetricLabels,
		),
	}

	if options.ListDot11SSIDs {
		metrics.advertisedSSIDs = newDot11DeviceSSIDsMetrics(metricsNamespace, devicesMetricLabels)
	}

	if options.ListDot11Clients {
		metrics.clients = newDot11DeviceClientsMetrics(metricsNamespace, devicesMetricLabels)
	}

	return metrics
}
