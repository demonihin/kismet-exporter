package devices

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	kismet "gitlab.com/demonihin/go-kismet-api"
)

const (
	devicesMetricsSubsystem             = "devices"
	devicesMetricsDot11Subsystem        = "devices_dot11"
	devicesMetricsDot11ClientsSubsystem = "devices_dot11_clients"
	devicesMetricsDot11SSIDsSubsystem   = "devices_dot11_ssids"
	devicesMetricsBluetoothSubsystem    = "devices_bluetooth"
)

type MetricsOptions struct {
	// Retention - means:
	// get current time and deduct the value (time.Now().Add(-Retention)).
	Retention time.Duration

	// ListDot11SSIDs - includes 802.11 SSID metrics.
	// This may cause
	// too high load on Kismet, exporter and Prometheus
	// because the cardinality of metrics might change very often.
	ListDot11SSIDs bool

	// ListDot11Clients bool - includes 802.11 client metrics. If it is false (default),
	// then only metrics for WiFi Access Points are exported.
	// This may cause
	// too high load on Kismet, exporter and Prometheus
	// because the cardinality of metrics might change very often.
	ListDot11Clients bool
}

type MetricsCollector struct {
	kismetClient *kismet.Client

	options MetricsOptions

	metrics *metrics
}

/*
Use almost all metrics from Dot11Device by default
which scale linear with the number of devices i.e. one value of each type per a device.
*/

func NewMetricsCollector(cl *kismet.Client, metricsNamespace string, options MetricsOptions) *MetricsCollector {
	var devicesMetrics = MetricsCollector{
		kismetClient: cl,
		options:      options,
		metrics:      newDeviceMetrics(metricsNamespace, options),
	}

	return &devicesMetrics
}

var _ prometheus.Collector = (*MetricsCollector)(nil)

func (dMC *MetricsCollector) Describe(descs chan<- *prometheus.Desc) {
	dMC.metrics.Describe(descs)
}

func (am *MetricsCollector) Collect(metrics chan<- prometheus.Metric) {
	// if err := am.collectAlerts(metrics); err != nil {
	// 	klog.ErrorS(err, "collectAlerts")
	// }
}
