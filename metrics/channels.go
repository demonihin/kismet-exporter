package metrics

import (
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
	kismet "gitlab.com/demonihin/go-kismet-api"
	"gitlab.com/demonihin/kismet-exporter/metrics/helpers"
	klog "k8s.io/klog/v2"
)

const channelsMetricsCollectorSubsystem = "channels"

type channelsMetricsCollector struct {
	kismetClient *kismet.Client

	signal              prometheus.GaugeVec
	packetsStatsLastMin prometheus.GaugeVec
	dataStatsLastMin    prometheus.GaugeVec
	devicesSeen         prometheus.GaugeVec
}

func newChannelsMetricsCollector(cl *kismet.Client) *channelsMetricsCollector {
	var channelMetricsLabels = []string{
		"channel",
		"frequency",
		"signal_type",
	}

	return &channelsMetricsCollector{
		kismetClient: cl,
		signal: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: channelsMetricsCollectorSubsystem,
			Name:      "signal",
			Help:      "Signal characteristics per channel",
		},
			helpers.AppendCopyLabels(channelMetricsLabels, []string{"metric"}),
		),
		packetsStatsLastMin: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: channelsMetricsCollectorSubsystem,
			Name:      "packets",
			Help:      "Sum of packets count during the last minute",
		},
			channelMetricsLabels,
		),
		dataStatsLastMin: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: channelsMetricsCollectorSubsystem,
			Name:      "data",
			Help:      "Sum of data bytes count during the last minute",
		},
			channelMetricsLabels,
		),
		devicesSeen: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: channelsMetricsCollectorSubsystem,
			Name:      "devices",
			Help:      "Devices seen at the moment of query",
		},
			channelMetricsLabels,
		),
	}
}

var _ prometheus.Collector = (*channelsMetricsCollector)(nil)

func (pm *channelsMetricsCollector) Describe(descs chan<- *prometheus.Desc) {
	pm.signal.Describe(descs)
	pm.packetsStatsLastMin.Describe(descs)
	pm.dataStatsLastMin.Describe(descs)
}

func (pm *channelsMetricsCollector) Collect(metrics chan<- prometheus.Metric) {
	if err := pm.collectChannels(metrics); err != nil {
		klog.ErrorS(err, "channelsMetricsCollector")
	}
}

func (pm *channelsMetricsCollector) collectChannels(metrics chan<- prometheus.Metric) error {
	channelsStats, err := pm.kismetClient.GetChannels()
	if err != nil {
		// Report error.
		metrics <- prometheus.NewInvalidMetric(
			prometheus.NewDesc(
				MetricsNamespace+"_"+channelsMetricsCollectorSubsystem,
				"Kismet API call error",
				nil,
				prometheus.Labels{},
			),
			err,
		)

		return fmt.Errorf("kismet.GetChannels: %w", err)
	}

	for freq := range channelsStats.FrequencyMap {
		var (
			info          = channelsStats.FrequencyMap[freq]
			signalInfo    = info.Signal
			signalType    = string(info.Signal.Type)
			signalChannel = info.Channel
		)

		pm.signal.WithLabelValues(signalChannel, freq, signalType, "carrierset").Set(float64(signalInfo.Carrierset))
		pm.signal.WithLabelValues(signalChannel, freq, signalType, "encodingset").Set(float64(signalInfo.Encodingset))
		pm.signal.WithLabelValues(signalChannel, freq, signalType, "max_seen_rate").Set(signalInfo.Maxseenrate)
		pm.signal.WithLabelValues(signalChannel, freq, signalType, "max_noise").Set(float64(signalInfo.MaxNoise))
		pm.signal.WithLabelValues(signalChannel, freq, signalType, "max_signal").Set(float64(signalInfo.MaxSignal))
		pm.signal.WithLabelValues(signalChannel, freq, signalType, "min_noise").Set(float64(signalInfo.MinNoise))
		pm.signal.WithLabelValues(signalChannel, freq, signalType, "min_signal").Set(float64(signalInfo.MinSignal))
		pm.signal.WithLabelValues(signalChannel, freq, signalType, "last_noise").Set(float64(signalInfo.LastNoise))
		pm.signal.WithLabelValues(signalChannel, freq, signalType, "last_signal").Set(float64(signalInfo.LastSignal))

		// Packets count.
		var packetsCount float64 // It is a bit weird to use float64 here but Kismet's debug JSON claims that they use double.
		for _, v := range info.PacketsRrd.MinuteVec {
			packetsCount += v
		}

		pm.packetsStatsLastMin.WithLabelValues(signalChannel, freq, signalType).Set(packetsCount)

		// Bytes count.
		var dataCount float64 // It is a bit weird to use float64 here but Kismet's debug JSON claims that they use double.
		for _, v := range info.DataRrd.MinuteVec {
			dataCount += v
		}

		pm.dataStatsLastMin.WithLabelValues(signalChannel, freq, signalType).Set(dataCount)

		// Devices count.
		// Due to the fact that Kismet usually performs scanning with channel change on adapters
		// the quantity of devices jumps up and down but I assume that
		// no more real devices could exist than the "max" of the devices seen
		// so I get max of the RRD Minute vector for devices count.
		var devicesCount float64 // It is a bit weird to use float64 here but Kismet's debug JSON claims that they use double.
		for _, v := range info.DeviceRrd.MinuteVec {
			if v > devicesCount {
				devicesCount = v
			}
		}

		pm.devicesSeen.WithLabelValues(signalChannel, freq, signalType).Set(devicesCount)
	}

	pm.signal.Collect(metrics)
	pm.packetsStatsLastMin.Collect(metrics)
	pm.dataStatsLastMin.Collect(metrics)
	pm.devicesSeen.Collect(metrics)

	return nil
}
