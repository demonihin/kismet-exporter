package metrics

import (
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
	kismet "gitlab.com/demonihin/go-kismet-api"
	klog "k8s.io/klog/v2"
)

const systemMetricsSubsystem = "system"

type systemMetricsCollector struct {
	kismetClient *kismet.Client

	// systemTimestamp - "kismet.system.timestamp.sec".
	timestamp prometheus.Gauge
	// timestampStart - "kismet.system.timestamp.start_sec".
	timestampStart prometheus.Gauge

	// sensorsTemp - "kismet.system.sensors.temp".
	sensorsTemp prometheus.GaugeVec
	// numHttpConnections - "kismet.system.num_http_connections".
	numHTTPConnections prometheus.Gauge

	// sensorsFan - "kismet.system.sensors.fan".
	sensorsFan prometheus.GaugeVec

	// serverTextInfo - "kismet.system.server_location", "kismet.system.server_description",
	// "kismet.system.server_name", "kismet.system.user", "kismet.system.user",
	// "kismet.system.version", "kismet.system.git", "kismet.system.build_time".
	serverTextInfo prometheus.GaugeVec

	// batteryPercentage - "kismet.system.battery.percentage".
	batteryPercentage prometheus.Gauge

	// batteryAC - "kismet.system.battery.ac".
	batteryAC prometheus.Gauge

	// memoryRSS - "kismet.system.memory.rss".
	memoryRSS prometheus.Gauge

	// devicesCount - "kismet.system.devices.count".
	devicesCount prometheus.Gauge
}

func newSystemMetricsCollector(cl *kismet.Client) *systemMetricsCollector {
	return &systemMetricsCollector{
		kismetClient: cl,
		timestamp: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "timestamp",
			Help:      "Current Kismet server timestamp seconds from epoch",
		}),
		timestampStart: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "timestamp_start",
			Help:      "Kismet server start timestamp",
		},
		),
		sensorsTemp: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "sensors_temp",
			Help:      "Temperature sensors",
		},
			[]string{"sensor_name"},
		),
		numHTTPConnections: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "http_connections",
			Help:      "HTTP connections count to Kismet",
		}),
		sensorsFan: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "sensors_fan",
			Help:      "Fan sensors",
		},
			[]string{"fan_name"},
		),
		serverTextInfo: *prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "server_info",
			Help:      "Server information",
		},
			[]string{"info_type", "info_value"},
		),
		batteryPercentage: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "battery_percentage",
			Help:      "Battery charge",
		}),
		batteryAC: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "battery_ac",
			Help:      "1 if the Kismet server has AC power source",
		}),
		memoryRSS: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "memory_rss",
			Help:      "RSS memory used by Kismet",
		}),
		devicesCount: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: MetricsNamespace,
			Subsystem: systemMetricsSubsystem,
			Name:      "devices_count",
			Help:      "Kismet detected devices count",
		}),
	}
}

var _ prometheus.Collector = (*systemMetricsCollector)(nil)

// Describe sends the super-set of all possible descriptors of metrics
// collected by this Collector to the provided channel and returns once
// the last descriptor has been sent. The sent descriptors fulfill the
// consistency and uniqueness requirements described in the Desc
// documentation.
//
// It is valid if one and the same Collector sends duplicate
// descriptors. Those duplicates are simply ignored. However, two
// different Collectors must not send duplicate descriptors.
//
// Sending no descriptor at all marks the Collector as “unchecked”,
// i.e. no checks will be performed at registration time, and the
// Collector may yield any Metric it sees fit in its Collect method.
//
// This method idempotently sends the same descriptors throughout the
// lifetime of the Collector. It may be called concurrently and
// therefore must be implemented in a concurrency safe way.
//
// If a Collector encounters an error while executing this method, it
// must send an invalid descriptor (created with NewInvalidDesc) to
// signal the error to the registry.
func (sm *systemMetricsCollector) Describe(descs chan<- *prometheus.Desc) {
	sm.batteryAC.Describe(descs)
	sm.batteryPercentage.Describe(descs)
	sm.devicesCount.Describe(descs)
	sm.memoryRSS.Describe(descs)
	sm.numHTTPConnections.Describe(descs)
	sm.sensorsFan.Describe(descs)
	sm.sensorsTemp.Describe(descs)
	sm.serverTextInfo.Describe(descs)
	sm.timestamp.Describe(descs)
	sm.timestampStart.Describe(descs)
}

// Collect is called by the Prometheus registry when collecting
// metrics. The implementation sends each collected metric via the
// provided channel and returns once the last metric has been sent. The
// descriptor of each sent metric is one of those returned by Describe
// (unless the Collector is unchecked, see above). Returned metrics that
// share the same descriptor must differ in their variable label
// values.
//
// This method may be called concurrently and must therefore be
// implemented in a concurrency safe way. Blocking occurs at the expense
// of total performance of rendering all registered metrics. Ideally,
// Collector implementations support concurrent readers.
func (sm *systemMetricsCollector) Collect(metrics chan<- prometheus.Metric) {
	if err := sm.collectSystemStatus(metrics); err != nil {
		klog.ErrorS(err, "systemMetrics")
	}
}

func (sm *systemMetricsCollector) collectSystemStatus(metrics chan<- prometheus.Metric) error {
	// Load system status.
	status, err := sm.kismetClient.GetSystemStatus(
		kismet.FieldSimplification{
			"kismet.system.num_http_connections",
			"kismet.system.sensors.temp",
			"kismet.system.sensors.fan",
			"kismet.system.server_location",
			"kismet.system.server_description",
			"kismet.system.server_name",
			"kismet.system.battery.percentage",
			"kismet.system.timestamp.sec",
			"kismet.system.timestamp.start_sec",
			"kismet.system.memory.rss",
			"kismet.system.devices.count",
			"kismet.system.user",
			"kismet.system.version",
			"kismet.system.git",
			"kismet.system.build_time",
		},
	)
	if err != nil {
		// Report error.
		metrics <- prometheus.NewInvalidMetric(
			prometheus.NewDesc(
				MetricsNamespace+"_"+systemMetricsSubsystem,
				"Kismet API call error",
				nil,
				prometheus.Labels{},
			),
			err,
		)

		return fmt.Errorf("kismet.GetSystemStatus: %w", err)
	}

	sm.batteryAC.Set(float64(status.SystemBatteryAC))
	sm.batteryAC.Collect(metrics)

	sm.batteryPercentage.Set(float64(status.SystemBatteryPercentage))
	sm.batteryPercentage.Collect(metrics)

	sm.devicesCount.Set(float64(status.SystemDevicesCount))
	sm.devicesCount.Collect(metrics)

	sm.memoryRSS.Set(float64(status.SystemMemoryRSS))
	sm.memoryRSS.Collect(metrics)

	sm.numHTTPConnections.Set(float64(status.SystemNumHTTPConnections))
	sm.numHTTPConnections.Collect(metrics)

	sm.timestamp.Set(float64(status.SystemTimestampSEC))
	sm.timestamp.Collect(metrics)

	sm.timestampStart.Set(float64(status.SystemTimestampStartSEC))
	sm.timestampStart.Collect(metrics)

	// Fan.
	for k, v := range status.SystemSensorsFan {
		sm.sensorsFan.WithLabelValues(k).Set(v)
	}
	sm.sensorsFan.Collect(metrics)

	// Temperature.
	for k, v := range status.SystemSensorsTemp {
		sm.sensorsTemp.WithLabelValues(k).Set(v)
	}
	sm.sensorsTemp.Collect(metrics)

	// Server info.
	sm.serverTextInfo.WithLabelValues("server_location", status.SystemServerLocation).Set(1)
	sm.serverTextInfo.WithLabelValues("server_description", status.SystemServerDescription).Set(1)
	sm.serverTextInfo.WithLabelValues("server_name", status.SystemServerName).Set(1)
	sm.serverTextInfo.WithLabelValues("user", status.SystemUser).Set(1)
	sm.serverTextInfo.WithLabelValues("version", status.SystemVersion).Set(1)
	sm.serverTextInfo.WithLabelValues("git", status.SystemGit).Set(1)
	sm.serverTextInfo.WithLabelValues("build_time", status.SystemBuildTime).Set(1)
	sm.serverTextInfo.Collect(metrics)

	return nil
}
