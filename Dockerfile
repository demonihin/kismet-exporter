FROM docker.io/library/golang:1.17.5-alpine3.15 as builder
WORKDIR /usr/src/
COPY . .
RUN cd cmd && go build -o /usr/src/kismet_exporter

FROM docker.io/library/alpine:3.15.0
COPY --from=builder /usr/src/kismet_exporter /kismet_exporter
CMD [ "/kismet_exporter" ]